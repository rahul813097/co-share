'use strict';

angular.module('myApp').controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
    var self = this;
    self.name='rahulkumar';
    self.cont=[];
   fetchAllContent();

   function fetchAllContent(){
       UserService.fetchAllContent()
           .then(
           function(d) {
               self.cont = d;
               console.log('fetched into controller');
           },
           function(errResponse){
               console.error('Error while fetching Users');
           }
       );
   }
    
   
  
}]);

