'use strict';

angular.module('myApp').factory('UserService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8080/client/get';

    var factory = {
        fetchAllContent: fetchAllContent
    };

    return factory;

    function fetchAllContent() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
                console.log('content fetched',response.data);
            },
            function(errResponse){
                console.error('Error while fetching Content');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }    
   

}]);
