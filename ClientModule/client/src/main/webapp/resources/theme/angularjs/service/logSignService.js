'use strict';

angular.module('myApp').factory('logSignService', ['$http', '$q', function($http, $q){
	var REST_SERVICE_URI = 'http://localhost:8080/admin/';
	var uri='http://localhost:8080/client/';
    var factory = {
        doLogin:doLogin  ,
        doSignin:doSignin
    };
   
        return factory;
        
function doLogin(logDetail)
{
	console.log('into doLogin of service');
	var deferred=$q.defer();
	$http.post(REST_SERVICE_URI+"login",logDetail).then(function(response){console.log('logged in service')
	deferred.resolve(response.data);	
	},function(errResponse)
			{
console.error('Error while loggin');
deferred.reject(errResponse);
			});
return deferred.promise;	
}

 function doSignin(user)
 {
	 console.log('into signing of service');
	 var deferred =$q.defer();
	 $http.post(REST_SERVICE_URI+"user",user).then(function(response){console.log('signed up');
	 deferred.resolve(response.data);
	 },
			 function(errResponse){console.log('error while signing');
			 deferred.reject(errResponse);		 
	 });
	 return deferred.promise;
 }
	
}]);
