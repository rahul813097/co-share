
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

<!--Nav Bar -->
<div class="col-md-12" style="background-color:#fafafa;height:63px">

	<div class="col-md-2">
		<div class="col-md-4" style="padding:14px;">
		<p><i class="material-icons" style="font-size:38px">check_box_outline_blank</i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 18px;">Coshare</p>
		</div>
	</div>

	<div class="col-md-1" style="float:right">
		<div class="col-md-6" style="padding:17px;">
		<p><i class="fa fa-sign-out" style="font-size: 28px;"></i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 13px;"><a href="login">Login</a></p>
		</div>
	</div>

	<div class="col-md-1" style="float:right">
		<div class="col-md-6" style="padding:17px;">
		<p><i class="fa fa-comments-o" style="font-size: 28px;"></i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 13px;"><a href="blog">Blog</a></p>
		</div>
	</div>

</div>
<!--//Nav Bar -->	


<!--image holder -->

<div class="container col-md-12" style="height: 169px;background:url(${pageContext.request.contextPath}/resources/images/snow.jpg);">
	<div class="col-md-6" style="padding: 13px;">
		<p style="font-size: 20px;color:  white;margin-top: 16px;">Pg/Flat selected in City</p>
	</div>

	<div class="col-md-offset-4 col-md-4">
			<select name="country" style="width: 100%; margin: 5px;  height:  37px; background-color: #ffffff;color:#5d5b5b;
			border-color:#ffffff">
			    <option value="australia">Choose your city</option>
			    <option value="australia">Delhi</option>
			    <option value="noida">Noida</option>
    		</select>
	</div>
</div>
<!--//image holder -->

<!--filters -->
<div class="col-md-12" style="background-color:#eaeaea">
	<div class="col-md-2" style="padding:16px">
		<h5>Apply Filters :</h5>
	</div>
	<div class="col-md-5">
		<div class="col-md-4" style="padding:16px">
			<h5>Number of BHK :</h5>
		</div>
		<div class="col-md-8" style="padding:10px">
			<select name="country" style="width: 100%; margin: 5px;  height:  37px; background-color: #ffffff;color:#5d5b5b;
			border-color:#ffffff">
			    <option value="australia">Choose your option</option>
			    <option value="australia">1</option>
			    <option value="noida">2</option>
    		</select>
		</div>
	</div>
	<div class="col-md-5">
		<div class="col-md-4" style="padding:16px">
			<h5>Available for :</h5>
		</div>
		<div class="col-md-8" style="padding:10px">
			<select name="country" style="width: 100%; margin: 5px;  height:  37px; background-color: #ffffff;color:#5d5b5b;
			border-color:#ffffff">
			    <option value="australia">Choose your option</option>
			    <option value="australia">Boys</option>
			    <option value="noida">Girls</option>
    		</select>
		</div>
	</div>
</div>
<!--//filters -->

<!--Main Div-->
<div class="container col-md-12" style="height:450px">
	<div class="col-md-10 col-md-offset-1" style="padding-top: 45px;">
		<div class="col-md-3">
			<h4>10 Coshares found</h4>
		</div>
		<div class="col-md-9" style="background-color:#eeeeee;border-bottom:4px solid #567dbe">
			<h4 style="text-align:center">Notification Area</h4>
			<p>Lorem ipsum dolor emet qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed  velit, sed r sit amet, co</p>
		</div>
	</div>

<div class="col-md-9 col-md-offset-2" style="background-color:#f5f5f5;margin-top: 40px;padding: 0px;border-bottom: 3px solid grey;">
		<div class="col-md-4" style="background-color:#eeeeee;padding:  0px;">
			<img src="${pageContext.request.contextPath}/resources/images/house.jpg" alt="house" style="width:100%;height: 190px;">
		</div>
		<div class="col-md-8">
			<div class="col-md-12" style="padding-top:20px">
				<div class="col-md-6" style="color: #3e3c3c;padding: 0px;">
					<div class="col-md-1"><i class="fa fa-map-pin" style="font-size:24px"></i></div>
					<div class="col-md-8"><p>location</p></div>
				</div>
				<div class="col-md-6">
				    <a href="#" style="float: right;">Get a call</a>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-12" style="padding-top: 15px;padding-bottom:8px">
					<p>location near to this address</p>
				</div>
				
			</div>
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="col-md-12" style="padding-bottom: 10px;border-right: 2px solid grey;">
						<i class="fa fa-child" style="font-size:36px"></i>
					</div>
					<p>For Boys</p>
				</div>
				<div class="col-md-4" style="padding-left: 50px;">
					<div class="col-md-12" style="padding-bottom: 10px;border-right: 2px solid grey;">
						<i class="material-icons" style="font-size:36px">kitchen</i>
					</div>
					<p>2nd Floor</p>
				</div>
				<div class="col-md-4" style="padding-left: 50px;">
					<div class="col-md-3">
						<i class="fa fa-rupee" style="font-size:36px"></i>
					</div>
					<div class="col-md-9" style="padding-bottom: 10px;">
						<p> 8000</p>
					</div>	
					<div class="col-md-12">
						<p>#Vacancies</p>
					</div>		
				</div>
			</div>
		</div>
	</div>

</div>
<!--//Main Div-->


<!--footer -->
<div class="container col-md-12" style="height:310px;background-color: #5d5b5b;margin-top:30px;">
	<div class="col-md-5 col-md-offset-1">
		<p style="color: white;font-size:21px;margin-top: 23px;">About Coshare</p>
		<p style="color: #dcdada;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
		 architecto beatae vitae dicta sunt explicabo.

		 Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
		voluptatem sequi nesciunt.

		 Neque porro quisquam est,
		 qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
		 quia non numquam eius


		 Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
		voluptatem sequi nesciunt.

		 Neque porro quisquam est,
		 qui dolorem ipsum quia 
		</p>
	</div>
	<div class="col-md-2 col-md-offset-1">
	 		<p style="color: white;font-size:21px;margin-top: 23px;">Company</p>
	 		<ul style=" padding: 12px; list-style:  none; color:  #dcdada">
				<li style="margin-bottom: 8px">Career</li>
				<li style="margin-bottom: 8px">About Us</li>
				<li style="margin-bottom: 8px">Privacy Policies</li>
				<li style="margin-bottom: 8px">Contact Us</li>
	 		</ul>
		</div>

	<div class="col-md-2 col-md-offset-1">
	 		<p style="color: white;font-size:21px;margin-top: 23px;">Explore</p>
	 		<ul style=" padding: 12px; list-style:  none; color:  #dcdada">
				<li style="margin-bottom: 8px">Blog</li>
				<li style="margin-bottom: 8px">Site Map</li>
				<li style="margin-bottom: 8px">FAQ's</li>
	 		</ul>
		</div>
</div>

<!--//footer -->
<!--Footer 2-->
<div class="container col-md-12" style="height:55px;background-color:#fafafa">
		<div class="col-md-6 col-md-offset-1">
		<p style="color: #5d5b5b;font-size: 15px;margin-top: 18px;">@ Coshare.com 2018 All right reserved.</p>
		</div>
		<div class="col-md-3 col-md-offset-2">
			<div class="col-md-4">
				<p style="color: #5d5b5b;font-size: 15px;margin-top: 18px;">Join Us :</p>
			</div>
			<div class="col-md-8" style="padding-top:10px">
				<i class="fa fa-facebook" style="padding:10px"></i>
				<i class="fa fa fa-google-plus" style="padding:10px"></i>
				<i class="fa fa-twitter" style="padding:10px"></i>
				<i class="fa fa-instagram" style="padding:10px"></i>
			</div>
		</div>
</div>
<!--//Footer 2-->
</body>
</html>