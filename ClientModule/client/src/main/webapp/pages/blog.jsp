<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet" href="<c:url value="/resources/css/blog.css"/>">
</head>
<body ng-app="myApp" ng-controller="UserController as ctrl">


	<!-- search bar start-->
	<div class="col-md-12 head">

		<div class="col-md-6 col-md-offset-3 ">

			<select style="width: 500px; margin-top: 30px;">

				<option value="city">city</option>
				<option value="Delhi">Delhi</option>
				<option value="Mumbai">Mumbai</option>
				<option value="Allahabad">Allahabad</option>
				<option value="Banglore">Banglore</option>
			</select>

		</div>

	</div>
	<!-- search bar end-->

	<!-- logo start-->
	<div class="col-md-12 "
		style="height:250px; background-image:url(${pageContext.request.contextPath}/resources/images/house.jpeg);">

		<div class="col-md-3" style="float: left; margin-top: 50px">
			<p>
				<i class="fa fa-rocket" style="font-size: 39px; color: ghostwhite;"></i>
			</p>
		</div>
		<div class="col-md-5 col-md-offset-2 text" style="margin-top: 100px">
			<div class="col-md-12">
				<h2>Co-Blog working</h2>
			</div>
			<div class="col-md-12 col-md-offset-1">
				<p style="margin-left: 0px;">Co-Share official blog</p>
			</div>
		</div>
		<div class="col-md-2 " style="float: right; margin-top: 60px">

			<a href="#" style="color: ghostwhite;">
				<div class="col-md-3">
					<span class="glyphicon glyphicon-log-in"
						style="color: ghostwhite; font-size: 20px;"></span>
				</div>
				<div class="col-md-6">
					<a href="login">Login</a>
				</div>
			</a>
		</div>



	</div>
	<!-- logo end-->

<!-- external -->

    <div class="col-md-10 col-md-offset-1">
    <!--left main-->
        <div class="col-md-7 "   >
            <div class="col-md-12 border shadow" ng-repeat="u in ctrl.cont" style="margin-top: 30px">
                
                <div class="col-md-12 " style=" margin-left: -2px;" >
                    <div class="col-md-12">
                    <h1 ng-bind="u.title"></h1><br></div>
                    
                    <div class="col-md-12 ">
                <img ng-src={{u.imagePath}} alt="Smiley face" style="width: 100%;height: 100%">
                </div>
                                    
                    <div class="col-md-12">
                    
                      <p ng-bind="u.story"></p></div>
                
                </div>
            </div>
        </div>   
        <!--left main-->
        <!--right main-->
        <div class="col-md-5" style="margin-top: 30px">
            <div class="col-md-8  shadow">
                <h3 style="width: 100%;height: 100%">Recent Posts</h3>
            </div>
            <div class="col-md-8  shadow" ng-repeat="u in ctrl.cont">
<ul style="list-style-type:none">
 <a href=""><li ng-bind="u.title"></li></a>
</ul>  
            </div>

        </div>   
           <!--right main-->
    
    </div>
    
<!-- external -->


	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/app.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/controller/user_controller.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/service/user_service.js" />"></script>


</body>

</html>