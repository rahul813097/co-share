<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<link href="<c:url value="/resources/css/bootstrap.css"/>" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

<!--Notification Bar -->
<div class="col-md-12" style="background-color:#eeeeee;height:100px">

	<div class="col-md-offset-1 col-md-1" style="margin-top: 28px;">
	<i class="fa fa-bullhorn" style="font-size:39px"></i>
	</div>
	
	<div class="col-md-9" style="margin-top: 30px;font-size: 21px;">
		<p>Had we got any notification to publish? It will be in this notification area</p>
	</div>
	
	<div style="float:right;" class="">
	<p style="margin-top: 35px;">X</p>
	</div>

</div>
	
<!-- //Notification Bar -->

<!--Nav Bar -->
<div class="col-md-12" style="background-color:#fafafa;height:63px">

	<div class="col-md-2">
		<div class="col-md-4" style="padding:14px;">
		<p><i class="material-icons" style="font-size:38px">check_box_outline_blank</i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 18px;">Coshare</p>
		</div>
	</div>

	<div class="col-md-1" style="float:right">
		<div class="col-md-6" style="padding:17px;">
		<p><i class="fa fa-sign-out" style="font-size: 28px;"></i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 13px;"><a href="login">Login</a></p>
		</div>
	</div>

	<div class="col-md-1" style="float:right">
		<div class="col-md-6" style="padding:17px;">
		<p><i class="fa fa-comments-o" style="font-size: 28px;"></i>
		</p></div>

		<div class="col-md-2">
			<p style="padding-top: 21px;font-size: 13px;"><a href="blog">Blog</a></p>
		</div>
	</div>

</div>
<!--//Nav Bar -->	


<!--image holder -->

<div class="container col-md-12" style="height:220px; background:url(${pageContext.request.contextPath}/resources/images/snow.jpg)">
	
	<div class="col-md-offset-4 col-md-6" style="padding: 13px;">
		<p style="font-size: 28px;color:  white;margin-top:  45px;">Some fancy words about coshare</p>
	</div>
<a href="search">
	<div class="col-md-offset-4 col-md-4">
			<select name="country" style="width: 100%; margin: 5px;  height:  37px; background-color: #ffffff;color:#5d5b5b;
			border-color:#ffffff">
			    <option value="australia">Choose your city</option>
			    <option value="australia">Delhi</option>
			    <option value="noida">Noida</option>
    		</select>
	</div>
	</a>
</div>
<!--//image holder -->


<!-- Why Coshare-->
<div class="container col-md-offset-5 col-md-5" style="padding-bottom:20px">
	<p style="font-size: 35px;padding-top: 37px;">Why Coshare?</p>
</div>

<div class="container col-md-offset-1 col-md-5" style="color:5d5b5b">
<div class="col-md-4"><i class="material-icons" style="font-size:122px">check_box_outline_blank</i></div>
<div class="col-md-8" style="
    font-size: 24px;
    padding: 24px;
"><p>Some explanatory words about service 1</p></div>
</div>

<div class="container col-md-5" style="color:5d5b5b">
<div class="col-md-4"><i class="material-icons" style="font-size:122px">wifi</i></div>
<div class="col-md-8" style="
    font-size: 24px;
    padding: 24px;
"><p>Some explanatory words about service 2</p></div>
</div>

<div class="container col-md-offset-1 col-md-5" style="color:5d5b5b">
<div class="col-md-4"><i class="material-icons" style="font-size:122px">local_parking</i></div>
<div class="col-md-8" style="
    font-size: 24px;
    padding: 24px;
"><p>Some explanatory words about service 3</p></div>
</div>

<div class="container col-md-5" style="color:5d5b5b">
<div class="col-md-4"><i class="material-icons" style="font-size:122px">adjust</i></div>
<div class="col-md-8" style="
    font-size: 24px;
    padding: 24px;
"><p>Some explanatory words about service 4</p></div>
</div>
<!--//Why cashare -->

<!--footer -->
<div class="container col-md-12" style="height:310px;background-color: #5d5b5b;margin-top:30px;">
	<div class="col-md-5 col-md-offset-1">
		<p style="color: white;font-size:21px;margin-top: 23px;">About Coshare</p>
		<p style="color: #dcdada;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
		 architecto beatae vitae dicta sunt explicabo.

		 Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
		voluptatem sequi nesciunt.

		 Neque porro quisquam est,
		 qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
		 quia non numquam eius


		 Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
		voluptatem sequi nesciunt.

		 Neque porro quisquam est,
		 qui dolorem ipsum quia 
		</p>
	</div>
	<div class="col-md-2 col-md-offset-1">
	 		<p style="color: white;font-size:21px;margin-top: 23px;">Company</p>
	 		<ul style=" padding: 12px; list-style:  none; color:  #dcdada">
				<li style="margin-bottom: 8px">Career</li>
				<li style="margin-bottom: 8px">About Us</li>
				<li style="margin-bottom: 8px">Privacy Policies</li>
				<li style="margin-bottom: 8px">Contact Us</li>
	 		</ul>
		</div>

	<div class="col-md-2 col-md-offset-1">
	 		<p style="color: white;font-size:21px;margin-top: 23px;">Explore</p>
	 		<ul style=" padding: 12px; list-style:  none; color:  #dcdada">
				<li style="margin-bottom: 8px">Blog</li>
				<li style="margin-bottom: 8px">Site Map</li>
				<li style="margin-bottom: 8px">FAQ's</li>
	 		</ul>
		</div>
</div>

<!--//footer -->

<!--Footer 2-->
<div class="container col-md-12" style="height:55px;background-color:#fafafa">
		<div class="col-md-6 col-md-offset-1">
		<p style="color: #5d5b5b;font-size: 15px;margin-top: 18px;">@ Coshare.com 2018 All right reserved.</p>
		</div>
		<div class="col-md-3 col-md-offset-2">
			<div class="col-md-4">
				<p style="color: #5d5b5b;font-size: 15px;margin-top: 18px;">Join Us :</p>
			</div>
			<div class="col-md-8" style="padding-top:10px">
				<i class="fa fa-facebook" style="padding:10px"></i>
				<i class="fa fa fa-google-plus" style="padding:10px"></i>
				<i class="fa fa-twitter" style="padding:10px"></i>
				<i class="fa fa-instagram" style="padding:10px"></i>
			</div>
		</div>
</div>
<!--//Footer 2-->
</body>
</html>