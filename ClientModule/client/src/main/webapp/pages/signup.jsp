<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet" href="<c:url value="/resources/css/signup.css"/>">
</head>
<body ng-app="myApp" ng-controller="logSignController as ctrl">
	<!--Nav Bar -->
	<div class="col-md-12 " style="background-color: #fafafa; height: 63px">

		<div class="col-md-2">
			<div class="col-md-4" style="padding: 14px;">
				<p>
					<i class="material-icons" style="font-size: 38px">check_box_outline_blank</i>
				</p>
			</div>

			<div class="col-md-2">
				<p style="padding-top: 21px; font-size: 18px;">Coshare</p>
			</div>
		</div>
	</div>
	<!--//Nav Bar -->
	<!-- signup box -->
	<div class="col-md-12" style="margin-top: 40px;">
		<div class="col-md-5 col-md-offset-1"
			style="margin-top: 96px; margin-right: -14px;">
			<div class="col-md-12  leftDiv shadow">
				<div class="col-md-12">

					<div class="col-md-6">
						<div class="col-md-3">
							<i class="fa fa-search" style="font-size: 24px"></i>
						</div>
						<div class="col-md-9">
							<p>Explore</p>
							<div class="col-md-12">
								<p>Look for the place that suits like no other</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-3">
							<i class="fa fa-wrench" style="font-size: 24px"></i>
						</div>
						<div class="col-md-9">
							<p>Maintainance</p>
							<div class="col-md-12">
								<p>You just move in left this part for us</p>
							</div>
						</div>

					</div>
				</div>
				<div class="col-md-12">

					<div class="col-md-6">
						<div class="col-md-3">
							<span class="glyphicon glyphicon-time" style="font-size: 24px"></span>
						</div>
						<div class="col-md-9">
							<p>You Stay</p>
							<div class="col-md-12">
								<p>Decide yourself the time to visit the place you like</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-3">
							<i class="fa fa-rocket" style="font-size: 24px"></i>
						</div>
						<div class="col-md-9">
							<p>Move In</p>
							<div class="col-md-12">
								<p>Choose your day and just move in to stay</p>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-12 ">

				<div class="col-md-10">
					<p>
						Already a Member?<a href="login">login</a>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-4 shadow">
			<div>
				<span>Sign In</span>
			</div>
			<div style="margin-top: 50px">
				<form>
					<span>Enter your name or Email</span><br> <input
						class="inputfield " type="text" ng-model="ctrl.user.email" placeholder="abc@xyz.com or abc"><br>
					<br> <span>Enter your FirstName</span><br> <input
						class="inputfield " type="text" ng-model="ctrl.user.firstName" placeholder="first name"><br>
						<br> <span>Enter your LastName</span><br> <input
						class="inputfield " type="text" ng-model="ctrl.user.lastName" placeholder="last name"><br>
					<br> <span>Enter your password</span> <br> <input
						class="inputfield " type="password" ng-model="ctrl.user.password" placeholder="abc"><br>
					<br>
					<br> 	<button style="width:400px" ng-click="ctrl.signin()">Sign In</button>

					<br>
					<br>

				</form>
			</div>
		</div>
	</div>
	<!-- signup box -->
	
<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/app.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/controller/logSignController.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/service/logSignService.js" />"></script>
</body>
</html>
