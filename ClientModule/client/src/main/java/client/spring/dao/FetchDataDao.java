package client.spring.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.QueryResults;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.mongodb.connection.QueryResult;

import client.spring.content.Content;
import client.spring.controller.FetchAllDataRestController;

@Service("fetch")
public class FetchDataDao {
	private static final Logger l=LogManager.getLogger(FetchDataDao.class);	
	Content c=null;
	List<Content> list=null;
	
public List<Content> fetchedAllDataDao()
{
	l.info("into  fectchalldatadao");
	
	try {
	BasicDAO<Content, ObjectId> basicDao=new BasicDAO<Content ,ObjectId>(Content.class,new MorphiaDAO().getDatastore());
QueryResults<Content>	qr=basicDao.find();
this.list=qr.asList();
 //this.c=l.get(0);
	}catch(Exception e) {
		l.info("into  fectchalldatadao Exception");
		return list;}
	l.info("into  fectchalldatadao return"+list.get(0));
	return list;
}

}
