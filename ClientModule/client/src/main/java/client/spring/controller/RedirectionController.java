package client.spring.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class RedirectionController {
	//@Autowired
	//private ScheduleTheTask scheduleTheTask;
	private static final Logger l=LogManager.getLogger(RedirectionController.class);
	@RequestMapping(value="/" , method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView start() {
	//for scheduler  //	scheduleTheTask.scheduleTask();
		l.info("redirecting / requ to login page ");
		return new ModelAndView("HomePage","m","login page");
	}
	@RequestMapping(value="/signup" , method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView signup() {
		l.info("redirecting requ to Signup page ");
		return new ModelAndView("signup","m","Signup page");
	}
	@RequestMapping(value="/login" , method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView login() {
		l.info("redirecting requ to login page ");
		return new ModelAndView("login","m","log page");
	}
	@RequestMapping(value="/blog" , method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView blog() {
		l.info("redirecting requ to blog page ");
		return new ModelAndView("blog","m","blog page");
	}
	@RequestMapping(value="/search" , method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView search() {
		l.info("redirecting requ to blog page ");
		return new ModelAndView("search","m","search page");
	}
}