package client.spring.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import client.spring.content.Content;
import client.spring.dao.FetchDataDao;
@RestController
public class FetchAllDataRestController {
	private static final Logger l=LogManager.getLogger(FetchAllDataRestController.class);
	@Autowired
	FetchDataDao fectch;
//	@Autowired
	//QuartzConfig quartz;
	@RequestMapping(value="/get" ,method = { RequestMethod.GET ,RequestMethod.POST },produces = { "application/json" })
public ResponseEntity<List<Content>> getContent()
{
		l.info("into controller");
		List<Content> msg=fectch.fetchedAllDataDao();
		l.info("after Execution"+msg);
		//quartz.startScheduler();
		return new ResponseEntity<List<Content>>(msg, HttpStatus.OK);
}
}
