package client.spring.content;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
@Entity("UploadedForBlog")
public class Content {
	@Id
	private ObjectId id;
private String title;
private String story;
private String imagePath;
public ObjectId getId() {
	return id;
}
public void setId(ObjectId id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getStory() {
	return story;
}
public void setStory(String story) {
	this.story = story;
}
public String getImagePath() {
	return imagePath;
}
public void setImagePath(String imagePath) {
	this.imagePath = imagePath;
}
public Content(String title, String story) {

	this.title = title;
	this.story = story;
}
public Content() {}
public Content(String imagepath)
{
setImagePath(imagepath);	
}


}
