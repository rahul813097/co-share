package client.spring.configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "client.spring")
public class ConfigurationFile extends WebMvcConfigurerAdapter {
	private static final Logger l = LogManager.getLogger(Configuration.class);
	@Bean
	public ViewResolver internalResourceViewResolver() {
		l.info("getting view ");
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/pages/");
		internalResourceViewResolver.setSuffix(".jsp");
		return internalResourceViewResolver;
	}
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry handler) {
		l.info("getting handler");
		handler.addResourceHandler("/resources/**").addResourceLocations("/resources/theme/");
	}
}
