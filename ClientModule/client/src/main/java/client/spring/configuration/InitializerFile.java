package client.spring.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import client.spring.controller.RedirectionController;

public class InitializerFile extends AbstractAnnotationConfigDispatcherServletInitializer {
	private static final Logger l=LogManager.getLogger(InitializerFile.class);

	@Override
	protected Class<?>[] getRootConfigClasses() {

		l.info("picket springConfig class from getRootConfigClasses");
		return new Class[] {ConfigurationFile.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		l.info("picket / from getServletMappings");
				return new String [] {"/"};
	}
}
