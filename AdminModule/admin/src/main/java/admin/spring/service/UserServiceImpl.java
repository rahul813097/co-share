package admin.spring.service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import admin.spring.dao.SignUPDao;
import admin.spring.model.User;

@Service("userService")
public class UserServiceImpl implements UserServices{
		
	public String saveUser(User user) {
		final Logger logger = LogManager.getLogger(UserServiceImpl.class);
		logger.info(" into UserService impl");
return new SignUPDao().dosignup(user);
	}

}
