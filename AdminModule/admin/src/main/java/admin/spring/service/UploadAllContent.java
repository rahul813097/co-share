package admin.spring.service;

import java.io.File;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import admin.spring.UploadImage.UploadImage;
import admin.spring.content.Content;
import admin.spring.dao.UploadAllDataDao;
@Service("upload")
public class UploadAllContent {
	public String uploadAllContent(String title, String story, MultipartFile file,String path) {
	
		String msg = new UploadImage().uploadImage(file,path);
		Content content = new Content();
		content.setStory(story);
		content.setTitle(title);
		content.setImagePath("/admin/resources/uploadedImages/"+file.getOriginalFilename());
		String smsg = new UploadAllDataDao().uploadAllDataDao(content);
		return msg + "<-image,title&story->" + smsg;
	}

}
