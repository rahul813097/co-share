package admin.spring.service;

import org.springframework.stereotype.Service;

import admin.spring.model.LoginDetail;

@Service("loginVerifier")
public class DoLoginVerify {
	LoginDetail loginDetail;

	public String verify(LoginDetail loginDetail) {
		return new admin.spring.daoForLogin.DbInteraction().checkUserDetail(loginDetail);

	}

}
