package admin.spring.controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import admin.spring.model.LoginDetail;
import admin.spring.service.DoLoginVerify;
@RestController
public class LoginRestFulController {
	private static final Logger logger = LogManager.getLogger(LoginRestFulController.class);
	@Autowired
	DoLoginVerify loginVerifier;

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> doLogin(@RequestBody LoginDetail loginDetail) {
		logger.info("into doLogin");

		String msg = loginVerifier.verify(loginDetail);
		return new ResponseEntity<String>(JSONObject.quote(msg), HttpStatus.OK);
	}
}