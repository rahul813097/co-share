package admin.spring.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import admin.spring.model.User;
import admin.spring.service.UserServices;

@RestController
public class SignupRestFulController {
	private static final Logger logger = LogManager.getLogger(SignupRestFulController.class);
	@Autowired
	UserServices userService;

	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> createUser(@RequestBody User user) {

		logger.info("into createUser");

		String msg = userService.saveUser(user);
		return new ResponseEntity<String>(JSONObject.quote(msg), HttpStatus.CREATED);
	}

}
