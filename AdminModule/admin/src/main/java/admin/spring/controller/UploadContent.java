package admin.spring.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import admin.spring.service.UploadAllContent;

@RestController
public class UploadContent {
	private static final Logger l = LogManager.getLogger(UploadContent.class);
	@Autowired
	private UploadAllContent upload;
	private static String msg="";

	@RequestMapping(value = "/upload", produces = { "application/json" }, method = { RequestMethod.GET,
			RequestMethod.POST })

	public ResponseEntity<String> getFile(@RequestParam String title, @RequestParam("file") MultipartFile file,
			@RequestParam String story, HttpServletRequest req) throws Exception {
		try {
String  path=req.getServletContext().getRealPath("/resources/theme/");
		this.msg=	upload.uploadAllContent(title, story, file,path);
		} catch (Exception e) {
			l.info("exception uploading context");
			return new ResponseEntity<String>(JSONObject.quote("exception uploading context"), HttpStatus.OK);
		}
		l.info("successfully uploaded content"+msg);
		return new ResponseEntity<String>(JSONObject.quote("success"+msg), HttpStatus.OK);
	}
}
