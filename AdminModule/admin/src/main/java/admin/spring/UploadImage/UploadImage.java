package admin.spring.UploadImage;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import admin.spring.controller.UploadContent;

public class UploadImage {
	private static final Logger l = LogManager.getLogger(UploadContent.class);

	public String uploadImage(MultipartFile file, String path) {
		try {
			byte[] bytes = file.getBytes();
			// String uploadFolder = System.getProperty("user.dir");
			// CodeSource codeSource=UploadImage.class.getProtectionDomain().getCodeSource();
			// String uploadFolder=new
			// File(codeSource.getLocation().toURI().getPath()).getParentFile().getPath();

			String fullpath = (path + File.separator + "uploadedImages/");
			File dir = new File(fullpath);
			if (!dir.exists())
				dir.mkdirs();
		String Uploadpath=fullpath+file.getOriginalFilename();
			l.info(Uploadpath);
			Path path1 = Paths.get(Uploadpath);
			Files.write(path1, bytes);
		} catch (Exception e) {
			l.info("error while loading image" + e);
			return "image has not uploaded";

		}
		return "image uploaded successfully";
	}

}
