package admin.spring.dao;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class MorphiaDao {
private Morphia morphia;
private MongoClient mongoClient;
private Datastore datastore;
public MorphiaDao()
{
	this.mongoClient=new MongoClient(Constant.ip,Constant.port);
	this.morphia=new Morphia();
	this.datastore=morphia.createDatastore(mongoClient, Constant.database);

}
public Morphia getMorphia() {
	return morphia;
}
public void setMorphia(Morphia morphia) {
	this.morphia = morphia;
}
public MongoClient getMongoClient() {
	return mongoClient;
}
public void setMongoClient(MongoClient mongoClient) {
	this.mongoClient = mongoClient;
}
public Datastore getDatastore() {
	return datastore;
}
public void setDatastore(Datastore datastore) {
	this.datastore = datastore;
}
	

}
