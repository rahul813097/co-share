package admin.spring.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;

import admin.spring.content.Content;

public class UploadAllDataDao {
	
public String uploadAllDataDao(Content content)
{
	try {
	BasicDAO<Content, ObjectId> basicDao=new BasicDAO<Content ,ObjectId>(Content.class,new MorphiaDao().getDatastore());
	basicDao.save(content);
	}catch(Exception e) {return "error on UploadAllDataDao class";}
	return "uploaded successfully on mongodb";
}

}
