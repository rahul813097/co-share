package admin.spring.configuration;

import java.io.File;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class InitializerFile extends AbstractAnnotationConfigDispatcherServletInitializer{
	
private static final Logger  l=LogManager.getLogger(InitializerFile.class);
private int maxUploadSizeInMb = 5 * 1024 * 1024;
	@Override
	protected Class<?>[] getRootConfigClasses() {
		l.info("configurationfile picked up");
		return new Class[]{ConfigurationFile.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		
		return new String[] {"/"};
	}
	 @Override
	    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
l.info("into customizeRegistration..Initializer.");
	        // upload temp file will put here
	        File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
	        // register a MultipartConfigElement
	        MultipartConfigElement multipartConfigElement =
	                new MultipartConfigElement(uploadDirectory.getAbsolutePath(), 
	                		maxUploadSizeInMb, maxUploadSizeInMb * 2, maxUploadSizeInMb / 2);

	        registration.setMultipartConfig(multipartConfigElement);

	    }
	
}
