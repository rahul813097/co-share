package admin.spring.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "admin.spring")
public class ConfigurationFile extends WebMvcConfigurerAdapter {
	private static final Logger  l=LogManager.getLogger(ConfigurationFile.class);
	@Bean
	public ViewResolver getViewObject()
	{
		InternalResourceViewResolver internalResourceViewResolver=new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/pages/");
		internalResourceViewResolver.setSuffix(".jsp");
		return internalResourceViewResolver;
	}
public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
{
	configurer.enable();
	}
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry)
{
	registry.addResourceHandler("/resources/**").addResourceLocations("/resources/theme/");
	}
/*
@Bean
public CommonsMultipartResolver multipartResolver() {
    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
    resolver.setDefaultEncoding("utf-8");
    return resolver;
}*/
/*
@Bean(name = "multipartResolver")
public CommonsMultipartResolver commnsmultipartResolver() {
    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
    resolver.setDefaultEncoding("utf-8");
    return resolver;
}*/

@Bean
public MultipartResolver multipartResolver() {
	l.info("now into spirngconfig multipartResolver");
	// CommonsMultipartResolver resolver=new CommonsMultipartResolver();
	  //  resolver.setDefaultEncoding("utf-8");
	    return new StandardServletMultipartResolver();
}


}
