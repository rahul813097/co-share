package admin.spring.daoForLogin;

import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

import admin.spring.model.LoginDetail;
import admin.spring.model.User;

public class DbInteraction {

	public String SaveUser(User user) {
		try {
			MongoDBWithMorphiaAPI mongoDBWithMorphiaAPI = new MongoDBWithMorphiaAPI();
			BasicDAO<User, ObjectId> basicdao = new BasicDAO<User, ObjectId>(User.class,
					mongoDBWithMorphiaAPI.getDatastore());
			basicdao.save(user);
		} catch (Exception e) {
			return "failed due to exception";
		}
		return "SignUp successfully";
	}

	public String checkUserDetail(LoginDetail loginDetail) {
		MongoDBWithMorphiaAPI mongoDBWithMorphiaAPI = new MongoDBWithMorphiaAPI();
		BasicDAO<User, ObjectId> basicdao = new BasicDAO<User, ObjectId>(User.class,
				mongoDBWithMorphiaAPI.getDatastore());
		Query<User> query = basicdao.createQuery().field("email").equal(loginDetail.getEmail());
		User user = query.get();
		if (user != null) {
			if (user.getEmail().equals(loginDetail.getEmail())) {
				if (user.getPassword().equals(loginDetail.getPassword())) {
					return "success";
				}
				return "server msg -> password is not matched with recode";
			}
			return "server msg -> email is  matched";
		} else {

			return "server msg -> user is null ," + "+User.toString()+";
		}
	}

}