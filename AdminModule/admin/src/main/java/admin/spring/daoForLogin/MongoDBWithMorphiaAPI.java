package admin.spring.daoForLogin;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class MongoDBWithMorphiaAPI {
private MongoClient mongoClient;
private Datastore datastore;
private Morphia morphia;
public MongoDBWithMorphiaAPI()
{
this.mongoClient=new MongoClient(DataBaseConstant.ip,DataBaseConstant.port);
this.morphia=new Morphia();
this.datastore=morphia.createDatastore(mongoClient, DataBaseConstant.dbName);
}
public MongoClient getMongoClient() {
	return mongoClient;
}
public void setMongoClient(MongoClient mongoClient) {
	this.mongoClient = mongoClient;
}
public Datastore getDatastore() {
	return datastore;
}
public void setDatastore(Datastore datastore) {
	this.datastore = datastore;
}
public Morphia getMorphia() {
	return morphia;
}
public void setMorphia(Morphia morphia) {
	this.morphia = morphia;
}

}
