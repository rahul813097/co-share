<nav class="main-menu">
		<ul>
			<li>
				<a href="adminpanel.jsp">
					<i class="fa fa-home nav_icon"></i>
					<span class="nav-text">
					Dashboard
					</span>
				</a>
			</li>
			
			<li>
				<a href="teachers.jsp">
					<i class="fa fa-group nav_icon"></i>
					<span class="nav-text">
					add teachers
					</span>
				</a>
			</li>
			
			<li>
				<a href="students.jsp">
					<i class="fa fa-graduation-cap nav_icon"></i>
					<span class="nav-text">
					add students
					</span>
				</a>
			</li>
			
			
			<li>
				<a href="tables.jsp">
					<i class="icon-table nav-icon"></i>
					<span class="nav-text">
					Tables
					</span>
				</a>
			</li>
                        
                        
			<li>
				<a href="graphs.jsp">
					<i class="fa fa-bar-chart nav_icon"></i>
					<span class="nav-text">
						Graphs
					</span>
				</a>
			</li>
                </ul>
		<ul class="logout">
			<li>
			<a href="../login.jsp">
			<i class="icon-off nav-icon"></i>
			<span class="nav-text">
			Logout
			</span>
			</a>
			</li>
		</ul>
		
	</nav>