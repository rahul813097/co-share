<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<title>Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<%@include file="admin-links.jsp"%>
</head>
<body class="dashboard-page">

	<%@include file="admin-nav.jsp"%>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access"> <i
				class="icon-proton-logo"></i> <i class="icon-reorder"></i>
			</a>
		</nav>
		<%@include file="admin-topnav.jsp"%>
		<div class="main-grid">

			<div class="social grid">
				<div class="grid-info">
					<div ng-app="myApp" ng-controller="UserController as ctrl">

						<form name="myForm" method="post"
							action="${pageContext.request.contextPath}/upload"
							enctype="multipart/form-data" action="upload">
							<div class="col-md-6 " id="d">
								<textarea name="title"
									style="width: 100%; margine-left: -684px; width: 530px; height: 32px; margin-bottom: 21px;"
									ng-model="ctrl.title" placeholder="enter the title here"> </textarea>
							</div>

							<div class=" col-md-12" style="margin-bottom: 21px;">
								<div class=" col-md-4">
									<span>choose photo to upload</span>
								</div>
								<div class=" col-md-3">
									<input type="file" ng-file-select="onFileSelect($files)"
										name="file" ng-model="fileToUpload">	
								</div>
                                     <div class="col-md-5">
					<img ng-src="{{fileToUpload}}" style="width: 200px; height:150px;" /></div>
							</div>
							<div class=" col-md-12">

								<textarea name="story"
									style="width: 100%; margine-left: -684px; width: 530px; height: 200px; margin-bottom: 100px;"
									placeholder="write something about the post..."></textarea>
							</div>
							<div class=" col-md-2">
								<!--  	<button ng-click="ctrl.submit()">Upload</button>-->
								<input type="submit" value="upload">
							</div>
						</form>
						
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>

	</section>
	<script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/app.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/controller/user_controller.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/angularjs/service/user_service.js" />"></script>


</body>
</html>
