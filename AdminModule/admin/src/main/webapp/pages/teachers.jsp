
<!DOCTYPE html>
<head>
<title>Staff members</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<%@include file="admin-links.jsp" %>
</head>
<body class="dashboard-page">
	
    <%@include file="admin-nav.jsp" %>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
		</nav>
                <%@include file="admin-topnav.jsp" %>
                
    <div class="main-grid">
            <div class="agile-grids">	
                    <!-- input-forms -->
                    <div class="grids">
                            <div class="progressbar-heading grids-heading">
                                    <h2>Staff Member: </h2>
                            </div>
        <div class="panel panel-widget forms-panel">
          <div class="forms">
            <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
                    <div class="form-title">
                            <h4>ID Form :</h4>
                    </div>
                    <div class="form-body">
                            <form method="get" id="form1">
                                <div class="form-group col-md-12"> 
                                <label>Teacher Branch</label>
                                 <select class="form-control" name="branch" id="branch" required>
                                  <option>Select one of the branch</option>
                                  <option value="Applied science">Applied Science</option>
                                  <option value="Bio-tech">Bio-tech</option>
                                  <option value="Chemical">Chemical</option>
                                  <option value="Civil">Civil</option>
                                  <option value="Computer Science">Computer Science</option>
                                  <option value="Electronics">Electronics</option>
                                  <option value="It">IT</option>
                                  <option value="Mechanical">Mechanical</option>
                                </select>
                                </div>
                                    <div class="form-group col-md-6" id="id-data"> 
                                            <label>Teacher ID</label> 
                                            <input type="text" name="id" class="form-control" required> 
                                    </div> 
                                    <div class="form-group col-md-6"> 
                                            <label>Password</label> 
                                            <input type="text" name="password" class="form-control" value="abc123" id="password" required>
                                    </div> 
                                     
                                <%-- course section   --%>  
                                <div class="col-md-12" style="padding: 7px 0 12px 17px"><h4>Allocate Courses</h4></div>
                                <div class="form-group col-md-6"> 
                                <label>Course 1</label>
                                 <select class="form-control subjects" name="course1" required>
                                  <option>You have to choose any branch first</option>
                                </select>
                                </div>
                                
                                <div class="form-group col-md-6"> 
                                <label>Course 2</label>
                                 <select class="form-control subjects" name="course2" id="course2">
                                  <option>You have to choose any branch first</option>
                                </select>
                                </div>
                                
                                <div class="form-group col-md-6"> 
                                <label>Course 3</label>
                                 <select class="form-control subjects" name="course3" id="course3">
                                  <option>You have to choose any branch first</option>
                                </select>
                                </div>
                                
                                <div class="form-group col-md-6"> 
                                <label>Course 4</label>
                                 <select class="form-control subjects" name="course4" id="course4">
                                  <option>You have to choose any branch first</option>
                                </select>
                                </div>
                                
                                <%-- //course section   --%>
                                
    <button type="submit" class="btn btn-default w3ls-button" name="submit" id="sumbit" value="submit" data-toggle="modal" data-target="#myModal">Save
    </button>
                                 <button type="reset" class="btn btn-default w3ls-button" style="float: right">Reset</button>
                                        </form> 
                                </div>
                        </div>
                </div>
					</div>
                                </div>
                        </div>
                                <div id="message"></div>
                </div>
                <%-- Notification modal--%>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Notification</h4>
      </div>
      <div class="modal-body">
          <h2 id="show"></h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                
                
		<!-- footer -->
		<div class="footer">
			<p>� 2016 Colored . All Rights Reserved . Design by <a href="#">yatin and niraj</a></p>
		</div>
		<!-- //footer -->
	</section>
	<script src="js/bootstrap.js"></script>
     
        <script>
             $('#form1').submit(function(e) {
               e.preventDefault();
                var formData = new FormData($("#form1")[0]);
                $.ajax({
                        url:'addteacher.jsp',
                        type:'post',            
                        data:formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success:function(x) 
                        {	   
                        $("#show").html(x);
                        },
                        error: function(e) 
                        {
                        $("#show").html("error" + e);
                        }
                });
                  });
        </script>
           <script>
         $(document).ready(function(){
            $("#branch").change(function(){
                var b=$(this).val();
                $.ajax({
                        url:'ajax.jsp',
                        type:'post',
                        data:{catid:b},
                        success:function(x) 
                        {                
                        $(".subjects").html(x);
                        },
                        error: function(e) 
                        {
                        $("#message").html("error" + e);
                        }
                });
                
            });

        });
        </script>

</body>
</html>
