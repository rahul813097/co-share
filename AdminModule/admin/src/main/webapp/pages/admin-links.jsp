<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/style.css"/>" rel='stylesheet' type='text/css' />
<!-- bootstrap-css -->
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="<c:url value="/resources/css/style1.css"/>" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="<c:url value="/resources/css/font.css"/>" type="text/css"/>
<link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="<c:url value="/resources/js/jquery-3.2.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/modernizr.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.cookie.js"/>"></script>
<!--skycons-icons-->
<script src="<c:url value="/resources/js/skycons.js"/>"></script>
<!--//skycons-icons-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">