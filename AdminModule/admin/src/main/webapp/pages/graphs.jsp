<!DOCTYPE html>
<head>
<title>Graphs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<%@include file="admin-links.jsp" %>
<script src="js/screenfull.js"></script>
</head>
<body class="dashboard-page">
 <%@include file="admin-nav.jsp" %>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
		</nav>
                <%@include file="admin-topnav.jsp" %>
		<div class="main-grid">
			<div class="agile-grids">
				<div class="chart-heading">
					<h2>Informative Graphs</h2>
				</div>
				<div class="agile-grid-left">
					<div class="w3l-chart col-md-5">
						<h3>Multiple Data Sets</h3>
						<div id="graph"></div>
					</div>
				</div>
				<div class="agile-grid-right">
					<div class="w3l-chart1 col-md-7">
						<h3>Single Data Set</h3>
						<div id="graph2"></div>
					</div>
					<div class="w3l-chart2 col-md-10" style="margin-left: 9%">
						<h3>Without plugin and different options:</h3>
                                                <div id="graph3" style="padding-left: 12%"></div>
					</div>
				</div>
				<div class="clearfix"> </div>
				<!-- //agile-grid-right -->
			</div>
		</div>

		<!-- footer -->
		<div class="footer">
			<p>� 2016 BCET . All Rights Reserved .</p>
		</div>
		<!-- //footer -->
	</section>
		<script>
			$(function(){
				$('#graph').graphify({
					//options: true,
					start: 'combo',
					obj: {
						id: 'ggg',
						width: 620,
						height: 375,
						xGrid: false,
						legend: true,
						title: 'Cse vs Ece',
						x:['mon','tue','wed','thrs','fri'],
                                                points: [
							[7, 26, 33, 74, 12],
							[32, 46, 75, 38, 62]
						],
						pointRadius: 3,
						colors: ['blue', 'red'],
						xDist: 70,
						dataNames: ['Cse', 'Ece'],
						xName: 'Day',
						tooltipWidth: 15,
						animations: true,
						pointAnimation: true,
						averagePointRadius: 10,
						design: {
							tooltipColor: '#fff',
							gridColor: 'grey',
							tooltipBoxColor: 'green',
							averageLineColor: 'green',
							pointColor: 'green',
							lineStrokeColor: 'grey',
						}
					}
				});
				$('#graph2').graphify({
					start: 'donut',
					obj: {
						id: 'lol',
						legend: false,
						showPoints: true,
						width: 775,
						legendX: 450,
						pieSize: 200,
						shadow: true,
						height: 400,
						animations: true,
						points: [50,10,20,10,0],
						xDist: 90,
						scale: 12,
						yDist: 35,
						grid: false,
						xName: 'Year',
						dataNames: ['Amount'],
						design: {
							lineColor: 'red',
							tooltipFontSize: '20px',
							pointColor: 'red',
							barColor: 'blue',
							areaColor: 'orange'
						}
					}
				});
				var bar = new GraphBar({
					attachTo: '#graph3',
					special: 'combo',
					height: 600,
					width: 640,
					yDist: 50,
					xDist: 70,
					showPoints: false,
					xGrid: false,
						legend: true,
						points: [
							[17, 21, 51, 74, 12, 49, 33],
							[32, 15, 75, 20, 45, 90, 52],
                                                        [21, 14, 54, 42, 11, 36, 43]
						],
						colors: ['red', 'orange','pink'],
						dataNames: ['Hot', 'Warm','cool'],
						xName: 'Day',
						tooltipWidth: 15,
						design: {
							tooltipColor: '#fff',
							gridColor: 'black',
							tooltipBoxColor: 'green',
							averageLineColor: 'blue',
						}
				});
				bar.init();
			});
		</script>
		<!-- //agile-grid-right -->
	<script src="js/bootstrap.js"></script>
	<!-- agile-grid-right -->
<script src="js/graph.js"></script>
<!-- //agile-grid-right -->
</body>
</html>
