<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"/>
  <link rel="stylesheet" href="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"/>">
  <link rel="stylesheet" href="<c:url value="/resources/css/froala_editor.css"/>">
  <link rel="stylesheet" href="<c:url value="/resources/css/froala_style.css"/>">
  <link rel="stylesheet" href="<c:url value="/resources/css/plugins/code_view.css"/>">
  <link rel="stylesheet" href="<c:url value="/resources/css/admin.css"/>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <style>
    body {
      text-align: center;
    }

    div#editor {
      width: 81%;
      margin: auto;
      text-align: left;
    }
  </style>
</head>
<body ng-app="myApp" ng-controller="UserController as ctrl">
<div class="container">
    <div class="row">
        <div >
<div>
            <div class="col-md-11 col-md-offset-1 account-wall">
            <p>{{ctrl.name}}</p>
 <form name="myForm" id="f">
<div class="col-md-4 " style=" margin-bottom: 21px;" id="d">
<textarea style="width:100%;margine-left:-684px" ng-model="ctrl.title" placeholder="enter the title here"> </textarea>  
</div>

<div class="col-sm-6 col-md-4" style=" margin-bottom: 21px;">
    <input type="file" name="fileToUpload" ng-model="ctrl.fileToUpload">
</div>

        <div id="editor" class="col-sm-7 col-md-4 col-md-offset-1">
   
      <textarea  id='edit' style="margin-top: 30px;" placeholder="Type some text" >
      </textarea>

<input type="submit" value="upload">
  </div>         
    </form>
    <p ng-bind="ctrl.title"></p>
 </div>
    </div>
       
        </div>
               
        
    </div>
    
</div>
  

   <script type="text/javascript" src="<c:url value="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/froala_editor.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/align.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/code_beautifier.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/code_view.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/draggable.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/lists.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/paragraph_format.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/paragraph_style.min.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/plugins/entities.min.js"/>"></script>
 <script>
      $(function(){
        $('#edit')
          .on('froalaEditor.initialized', function (e, editor) {
            $('#edit').parents('form').on('submit', function () {
              console.log($('#edit').val());
              angular.element(document.getElementById('f')).scope().call($('#edit').val());
              return false;
            })
          })
          .froalaEditor({enter: $.FroalaEditor.ENTER_P, placeholderText: null})
      });
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
  <script type="text/javascript" src="<c:url value="/resources/angularjs/app.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/angularjs/controller/user_controller.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/angularjs/service/user_service.js" />"></script>
</body>
</html>