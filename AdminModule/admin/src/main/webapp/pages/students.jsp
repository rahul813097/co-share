<!DOCTYPE html>
<head>
<title>Students</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<%@include file="admin-links.jsp" %>
</head>
<body class="dashboard-page">
	
    <%@include file="admin-nav.jsp" %>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
		</nav>
                <%@include file="admin-topnav.jsp" %>
    <div class="main-grid">
            <div class="agile-grids">	
                    <!-- input-forms -->
                    <div class="grids">
                            <div class="progressbar-heading grids-heading">
                                    <h2>Students: </h2>
                            </div>
        <div class="panel panel-widget forms-panel">
                <div class="forms">
                        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
                                <div class="form-title">
                                        <h4>ID distribution Form :</h4>
                                </div>
                                <div class="form-body">
                                        <form method="get" id="form1">
                                            <div class="form-group col-md-12"> 
                                                        <label>Batch (Active Years)</label> 
                                                        <input type="text" name="batch" class="form-control" placeholder="yyyy-yyyy" required> 
                                                </div>
                                            <div class="form-group col-md-6"> 
                                            <label>Branch</label>
                                             <select class="form-control" name="branch" required>
                                              <option value="">Select one of the branch</option>
                                              <option value="bio-tech">Bio-tech</option>
                                              <option value="chemical">Chemical</option>
                                              <option value="civil">Civil</option>
                                              <option value="Computer Science">Computer Science</option>
                                              <option value="Electronics">Electronics</option>
                                              <option value="it">IT</option>
                                              <option value="Mechanical">Mechanical</option>
                                            </select>
                                            </div>
                                            
                                                <div class="form-group col-md-6"> 
                                                        <label>Number of Students</label> 
                                                        <input type="text" name="count" class="form-control" required> 
                                                </div> 
                                            
                                                <div class="form-group col-md-6"> 
                                                        <label>Initial ID</label> 
                                                        <input type="text" name="id" class="form-control" required> 
                                                </div> 
                                                <div class="form-group col-md-6"> 
                                                        <label>Constant Password (static for all IDs)</label> 
                                                        <input type="text" name="password" class="form-control" value="xyz123" required>
                                                </div> 
    <button type="submit" class="btn btn-default w3ls-button" name="submit" id="sumbit" value="submit" data-toggle="modal" data-target="#myModal">Save</button>
             <button type="reset" class="btn btn-default w3ls-button" style="float: right">Reset</button>
                                        </form> 
                                </div>
                        </div>
                </div>
					</div>
                                </div>
                        </div>
                </div>
                <%-- Notification modal--%>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Notification</h4>
      </div>
      <div class="modal-body">
          <h2 id="show"></h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                
                
		<!-- footer -->
		<div class="footer">
			<p>� 2016 All Rights Reserved . Design by <a href="#">yatin and niraj</a></p>
		</div>
		<!-- //footer -->
	</section>
	<script src="js/bootstrap.js"></script>
        <script>
             $('#form1').submit(function(e) {
               e.preventDefault();
                var formData = new FormData($("#form1")[0]);
                $.ajax({
                        url:'addstudents.jsp',
                        type:'post',            
                        data:formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success:function(x) 
                        {	   
                        $("#show").html(x);
                        },
                        error: function(e) 
                        {
                        $("#show").html("error" + e);
                        }
                });
                  });
        </script>
	
</body>
</html>
