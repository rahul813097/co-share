'use strict';

angular.module('myApp').controller('UserController',['$scope','UserService',function($scope, UserService) {
					var self = this;
					self.user = {
						firstName : '',
						lastName : '',
						email : '',
						password : ''
					};
					self.users = [];
					self.submit = submit;
					self.reset = reset;
					
					function fetchAllUsers() {
						console.log('fetch all user into controller');
						UserService.fetchAllUsers().then(function(d) {
							self.users = d;
						}, function(errResponse) {
							console.error('Error while fetching Users controller');
						});
					}
					
					function createUser(user) {
						console.log('into create user of controller');
						UserService.createUser(user)
						.then(function(response) {
							self.signupmsg=response;
									console.log('reterieved data',response);
								},
										function(errResponse) {
											console.error('Error while creating User controller');
										}
								);}	
					
					
						function submit() {
								console.log('Saving New User submit', self.user);
								createUser(self.user);
								
							reset();
						}	
					function reset() {
						self.user = {
							firstName : '',
							lastName : '',
							email : '',
							password: ''
						};
						$scope.myForm.$setPristine(); //reset Form
					}

				} ]);



angular.module('myApp').controller('UserController1',['$scope','$location','UserService',function($scope,$location,UserService) {
var my=this;
	my.UserDetail={email:'',password:''};
	my.logreset=logreset;
	my.login=login;
	my.doLogin=doLogin;
	
	 function login()
		{
		console.log('into login',my.UserDetail);
		my.doLogin(my.UserDetail);
			my.logreset();
		}
		function doLogin(UserDetail)
		{
			console.log('into doLogin of controller',UserDetail);
			UserService.doLogin(UserDetail).then(function(data){
				my.loginmsg=data;
				if(data==="success")
					{
					console.log('into controller',$location);
					window.location.href="http://localhost:7002/admin/pages/adminpanel.jsp";
					}
							
			},
				function(errResponse) {
					console.error('Error while creating User controller');
				});
			}
		
		function logreset() {
			my.userDetail = {
				email : '',
				password: ''
			};
			$scope.myForm.$setPristine(); //reset Form
		}


} ]);

angular.module('myApp').controller('FileUploadController',['$scope','$http',function($scope,$http) {
var uploadUrl='http://localhost:7002/AdvanceCar/';

$scope.uploadFile = function(files) {
    var fd = new FormData();
    //Take the first selected file
    fd.append("file", files[0]);
    console.log('working');
$http.post(uploadUrl+"file",fd).then(function(response){
	console.log('successfully came from server',response);},
	function(errResponse){
	console.log('into error from servere',errResponse);
	
});
};

} ]);


	
