'use strict';
angular.module('myApp').factory('UserService',['$http', '$q', function($http, $q) {

			var REST_SERVICE_URI = 'http://localhost:7002/admin/';

			var factory = {
				fetchAllUsers : fetchAllUsers,
				createUser : createUser,
				doLogin: doLogin
			};

			return factory;

			function fetchAllUsers() {
				console.log('into fetch user');
				var deferred = $q.defer();
				$http.get(REST_SERVICE_URI).then(function(response) {
					deferred.resolve(response.data);
				}, function(errResponse) {
					console.error('Error while fetching Users');
					deferred.reject(errResponse);
				});
				return deferred.promise;
			}
			
			function createUser(user) {
				console.log('create user of service');
				var deferred=$q.defer();
				$http.post(REST_SERVICE_URI+"user", user).then(function(response) {
					console.log('reterieved data');
					deferred.resolve(response.data);
				},
				function(errResponse) {
					console.error('Error while creating User service');
					deferred.reject(errResponse);
				}
				);
				return deferred.promise;
			}
			
			function doLogin(ulogin)
			{
				console.log('into dologin of service',ulogin);
				var def=$q.defer();
				$http.post(REST_SERVICE_URI+"login",ulogin).then(function(response) {
					console.log('successfully came form service into service',response.data);
					def.resolve(response.data);
				}, 
				function(errResponse) {
					console.error('error in logging in service');
					def.reject(errResponse);
					});
				console.log('do login completed into service');
				return def.promise;
			}

		

		} ]);
