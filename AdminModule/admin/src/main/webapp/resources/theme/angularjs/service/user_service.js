'use strict';

angular.module('myApp').factory('UserService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:7002/admin/';

    var factory = {
        fetchAllUsers: fetchAllUsers,
              createUser: createUser,       
    };

    return factory;

    function fetchAllUsers() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
  //...
    

    //...

    function createUser(title,image) {
    	console.log('photo  services',image);
    	var fd=new FormData();
    	  fd.append('file', image);
        var deferred = $q.defer();
        $http({
            url: REST_SERVICE_URI+'upload',
            method: 'POST',
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},      
            data:image      
        })
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while Uploading');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
   //.....
        //....
    
    

}]);
