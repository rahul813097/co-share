'use strict';

angular.module('myApp').controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
    var self = this;
    self.name='rahulkumar';
    self.title='';
    $scope.fileToUpload='';
    
    self.submit = submit;
      self.reset = reset;
      self.createUser=createUser;
self.data='';

	$scope.call=function(data)
	{
		console.log(data);
		submit(data);
	}
	function createUser(title,fileToUpload) {
		console.log('into create user of controller',title,fileToUpload);
		UserService.createUser(title,fileToUpload)
		.then(function(response) {
					console.log('reterieved data in angcontroller',response);
				},
						function(errResponse) {
							console.error('Error while Uploading controller');
						}
				);
				
				}
    
    
    function submit() {
        
            console.log('Saving New data', self.title);
           
            console.log($scope.fileToUpload,'image');
            createUser(self.title,$scope.fileToUpload);
            
        reset();
    }
    
    function reset(){
    	  self.title='';
    	    self.fileToUpload='';
    	    self.wyeditor='';
        $scope.myForm.$setPristine(); //reset Form
    }

}]);


//...................................................


angular.module('myApp').directive("ngFileSelect", function(fileReader, $timeout) {
    return {
      scope: {
        ngModel: '='
      },
      link: function($scope, el) {
        function getFile(file) {
          fileReader.readAsDataUrl(file, $scope)
            .then(function(result) {
              $timeout(function() {
                $scope.ngModel = result;
              });
            });
        }

        el.bind("change", function(e) {
          var file = (e.srcElement || e.target).files[0];
          getFile(file);
        });
      }
    };
  });

angular.module('myApp').factory("fileReader", function($q, $log) {
  var onLoad = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.resolve(reader.result);
      });
    };
  };

  var onError = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.reject(reader.result);
      });
    };
  };

  var onProgress = function(reader, scope) {
    return function(event) {
      scope.$broadcast("fileProgress", {
        total: event.total,
        loaded: event.loaded
      });
    };
  };

  var getReader = function(deferred, scope) {
    var reader = new FileReader();
    reader.onload = onLoad(reader, deferred, scope);
    reader.onerror = onError(reader, deferred, scope);
    reader.onprogress = onProgress(reader, scope);
    return reader;
  };

  var readAsDataURL = function(file, scope) {
    var deferred = $q.defer();

    var reader = getReader(deferred, scope);
    reader.readAsDataURL(file);

    return deferred.promise;
  };

  return {
    readAsDataUrl: readAsDataURL
  };
});