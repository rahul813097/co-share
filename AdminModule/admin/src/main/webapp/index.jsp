<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
      <link rel="stylesheet" href="<c:url value="/resources/indexCss/style.css"/>"/>
  
</head>

<body ng-app="myApp">

  <div class="form"  >
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Sign Up </h1>
          
          <form name="myForm"  ng-controller="UserController as ctrl">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req" >*</span>
              </label>
              <input type="text" required autocomplete="off" name="firstName" ng-model="ctrl.user.firstName"/>
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name="lastName" ng-model="ctrl.user.lastName"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name="email" ng-model="ctrl.user.email"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name="password" ng-model="ctrl.user.password"/>
          </div>
          
          <button type="submit" class="button button-block" ng-click="ctrl.submit()">Get Started</button>
          
          <div ng-bind="ctrl.signupmsg"></div>
          
          </form>

        </div>
        <div id="login">   
          <h1>Welcome Back!</h1>
          
          <form  method="get" name="myForm" ng-controller="UserController1 as ctrl" >
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" ng-model="ctrl.UserDetail.email"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" ng-model="ctrl.UserDetail.password"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block" ng-click="ctrl.login()">Log In</button>
          <div ng-bind="ctrl.loginmsg"></div>
          </form>
        </div>
      </div>
</div> 
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script type="text/javascript" src="<c:url value="/resources/indexjs/index.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/app.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/controller/user_controller.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/service/user_service.js" />"></script>
</body>
</html>